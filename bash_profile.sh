
export EDITOR=vim
export HISTFILESIZE=20000
export HISTSIZE=10000
export PROMPT_COMMAND="history -a;$PROMPT_COMMAND";
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

SSH_ENV="$HOME/.ssh/environment"
PATH="$PATH:~/dot/bin:~/bin"

function start_agent {
     echo "Initialising new SSH agent..."
     /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
     echo succeeded
     chmod 600 "${SSH_ENV}"
     . "${SSH_ENV}" > /dev/null
     /usr/bin/ssh-add
     /usr/bin/ssh-add ~/.ssh/*rsa;
}

sk () {
    ssh-copy-id $1 && ssh $1
}

sc () {
    ssh-keygen -R $1
    ssh-keygen -R `host $1 | awk '{ print $4 }'`
}

rd () {
    rdesktop -u jnt6 -d WIN -z -a 16 -g 1280x1024 $1
}

rdw () {
    rdesktop -u jnt6 -d WIN -z -a 16 -g 1400x1050 $1
}

rdf () {
    rdesktop -u jnt6 -d WIN -z -a 16 -g 1920x1200 $1
}

g32 () {
    cat grouper.py > grouper_python2.py; 3to2 -n -w grouper_python2.py
}

crh () {
    cd ~/mnt/ssh/${1}/home/jnt6
}

rv () {
    vim --servername "${P}" --remote "${@:1}"
}

rvo () {
    vim --servername "${1}" --remote "${@:1}"
}

drake () {
    docker-compose exec web bundle exec rake "${@:1}"; docker-compose exec web sh -c 'chown -R 1000 /var/www/current/*'
}

drails () {
    docker-compose exec web bundle exec rails "${@:1}"; docker-compose exec web sh -c 'chown -R 1000 /var/www/current/*'
}

dundle () {
    docker-compose exec web bundle "${@:1}"; docker-compose exec web sh -c 'chown -R 1000 /var/www/current/*'
}

rs () {
    find * -type f -exec sed -i "s/${1}/${2}/g" {} +
}

# not all systems understand the TMUX term type, so use screen-256color when ssh'ing
if echo $TERM | grep tmux > /dev/null; then
    ssh () {
        COOLTERM=$TERM
        REALSSH=`which ssh`
        export TERM="screen-256color"; ${REALSSH} "${@}"; export TERM="${COOLTERM}"
    }
fi

alias grepc="grep --color=always"
alias lessc="less -r"
alias code="cd ~/code"
alias svi="sudoedit"

if uname -n|grep jnt6-lt-00 > /dev/null 2>&1; then
    PS1='[\D{%T} \u@\h \W$(__git_ps1 " (%s)")]\$ '
    alias cw="cd ~/code/clockworks"
    alias aka="cd ~/code/alsoknownas"
    alias hpc="cd ~/code/hpcmanage"
    alias steve="cd ~/code/stevedore"
    . /usr/share/git/completion/git-completion.bash
    . /usr/share/git/completion/git-prompt.sh
# Source SSH settings, if applicable
    if [ -f "${SSH_ENV}" ]; then
         . "${SSH_ENV}" > /dev/null
         #ps ${SSH_AGENT_PID} doesn't work under cywgin
         ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
             start_agent;
         }
    else
         start_agent;
    fi
fi
# powerline shell

#function _update_ps1() {
#    PS1="$(~/dot/powerline-shell/powerline-shell.py $? 2> /dev/null)"
#}

#if [ "$TERM" != "linux" ]; then
#    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
#fi
