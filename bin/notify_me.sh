#!/bin/bash

INFO=$(cat "$1")
CODE=$(echo "$INFO" | head -1)
TYPE=$(echo "$INFO" | head -2 | tail -1)
APP=$(basename $(dirname "$1"))
BODY=$(echo "$INFO" | tail -1 )

if [ "$CODE" == "success" ]; then
    true
else
    notify-send --hint=int:transient:1 -a "$APP" --icon=dialog-error "$APP $TYPE $CODE" "$BODY"
fi
