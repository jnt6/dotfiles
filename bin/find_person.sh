#!/bin/bash

FIRSTNAME=$1
LASTNAME=$2
MAIL=`echo "${FIRSTNAME}.${LASTNAME}@duke.edu" | tr '[A-Z]' '[a-z]'`
MORE_MAIL=`echo "${FIRSTNAME}@duke.edu" | tr '[A-Z]' '[a-z]'`

#ldapsearch -x '(|(&(sn='"${LASTNAME}"'*)(givenName='"${FIRSTNAME}"'*))(displayName='"${FIRSTNAME} ${LASTNAME}"')(mail='"${MAIL}"')(cn='"${FIRSTNAME} ${LASTNAME}"'))'
LDAPTLS_REQCERT=allow ldapsearch -b 'dc=duke,dc=edu' -H ldaps://ldap.duke.edu:636 -x '(|(&(sn='"${LASTNAME}"'*)(givenName='"${FIRSTNAME}"'*))(displayName='"${FIRSTNAME} ${LASTNAME}"')(duLDAPKey='"${FIRSTNAME}"')(uid='"${FIRSTNAME}"')(mail='"${MAIL}"')(mail='"${MORE_MAIL}"')(eduPersonPrincipalName='"${FIRSTNAME}"@duke.edu')(duDukeId='"${FIRSTNAME}"')(uidNumber='"${FIRSTNAME}"')(cn='"${FIRSTNAME} ${LASTNAME}"'))'
