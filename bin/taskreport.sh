#!/bin/bash

# uses: https://github.com/pixelb/scripts/commits/master/scripts/ansi2html.sh

# (only solution I found which gives you true color output)

(echo "Last update at `date`" && task rc._forcecolor=on ghistory && task rc._forcecolor=on summary && task rc._forcecolor=on list && task rc._forcecolor=on burndown) | ansi2html --palette=solarized > /tmp/cool.html && cp /tmp/cool.html /winhomes/jnt6/public_html/status.html
