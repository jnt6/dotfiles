#!/bin/sh
for policy in /sys/devices/system/cpu/cpufreq/policy*
do
    echo "power" > "$policy"/energy_performance_preference
done
