#!/bin/zsh

# this shell script is run on the dev workstation to set up the environment

echo "Put in DUO Pass Code:"
read -r DUO_PASSCODE
echo "CODE: ${DUO_PASSCODE}"
DUO_PASSCODE="${DUO_PASSCODE}" ssh-copy-id $1
echo "Put in DUO Pass Code:"
read -r DUO_PASSCODE
echo "CODE: ${DUO_PASSCODE}"
DUO_PASSCODE="${DUO_PASSCODE}" rsync -av ~/dotssh_remote/ ${1}:.ssh || (echo "couldn't do the rsync"; exit 2)
echo "Put in DUO Pass Code:"
read -r DUO_PASSCODE
echo "CODE: ${DUO_PASSCODE}"
DUO_PASSCODE="${DUO_PASSCODE}" ssh $1 -C 'git clone https://gitlab.oit.duke.edu/jnt6/dotfiles.git ~/dot && ~/dot/bin/link_dotfiles.sh'
