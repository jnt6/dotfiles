#!/bin/bash

cd ~/dot || (echo "can't cd to dot directory"; exit 2)
git submodule foreach git pull origin master
git submodule update --init
mkdir ~/dotbak
for df in `ls -ad .* | egrep -v '^(\.+$|\.git$)'`; do
    mv ~/${df} ~/dotbak/
    cd ~
    ln -s ~/dot/${df} ~/${df}
done

echo '[[ -f ~/dot/bash_profile.sh  ]] && . ~/dot/bash_profile.sh' >> ~/.bash_profile

mkdir ~/.vim_swapfiles
