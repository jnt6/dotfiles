#!/bin/bash

#export IFS="\n"
cat /tmp/adhoc.csv | while read line; do
    #echo -n "${line}" | cut -f5 -d,
    NETID=$(echo "${line}" | cut -f5 -d,)
    MAIL=$(LDAPTLS_REQCERT=allow ldapsearch -b 'ou=People,dc=duke,dc=edu' -H ldaps://ldap.duke.edu:636 -x '(eduPersonPrincipalName='"${NETID}"')' mail | grep 'mail:' | awk '{ print $2 }')
    echo "${line},${MAIL}" >> /tmp/emaild.csv
    # result = CovidTest.data_source_redcap.positive.where('requested_at > ?', Date.current - 90.days).joins(:student).pluck( 'students.unique_id', 'requested_at', 'students.sn', 'students.given_name', 'students.netid')
    
    #echo ${NETID}
done
#FIRSTNAME=$1
#LASTNAME=$2
#MAIL=`echo "${FIRSTNAME}.${LASTNAME}@duke.edu" | tr '[A-Z]' '[a-z]'`
#MORE_MAIL=`echo "${FIRSTNAME}@duke.edu" | tr '[A-Z]' '[a-z]'`

#ldapsearch -x '(|(&(sn='"${LASTNAME}"'*)(givenName='"${FIRSTNAME}"'*))(displayName='"${FIRSTNAME} ${LASTNAME}"')(mail='"${MAIL}"')(cn='"${FIRSTNAME} ${LASTNAME}"'))'
