#!/bin/bash

while true; do
    echo -n "$(date) "; curl -m 30.9 -w "remote_ip:  %{remote_ip} time_total:  %{time_total}s\n" -o /dev/null -k -X GET --header 'Accept: application/json' --header "Authorization: ${RUBRIK_TOKEN}" 'https://rubrik-prod-01.oit.duke.edu/api/v1/sla_domain/e35cb453-c5de-4f63-b3ed-fd69f0596992' 2>/dev/null
done
